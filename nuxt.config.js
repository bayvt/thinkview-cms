import CompressionPlugin from 'compression-webpack-plugin'
require('dotenv').config()

export default {
  mode: 'spa',
  head: {
    title: 'ThinkView-CMS',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  loading: { color: '#fff' },
  css: ['~/assets/scss/element-ui.scss'],
  plugins: [
    '~/plugins/persistedState',
    '~/plugins/element-ui',
    '~/plugins/components',
    '~/plugins/axios',
    '~/plugins/vue-croppa'
  ],
  buildModules: ['@nuxtjs/eslint-module', '@nuxtjs/tailwindcss'],
  modules: ['@nuxtjs/axios', '@nuxtjs/auth', '@nuxtjs/pwa', '@nuxtjs/dotenv'],
  build: {
    transpile: [/^element-ui/],
    extractCSS: true,
    extend(config, ctx) {},
    postcss: {
      preset: {
        autoprefixer: {
          flexbox: true
        }
      }
    },

    plugins: [new CompressionPlugin()]
  },
  // config modules
  tailwindcss: {
    exposeConfig: true
  },
  axios: {
    proxy: true
  },
  proxy: {
    '/cms/': { target: process.env.BASE_URL }
  },
  auth: {
    redirect: {
      login: '/auth/login',
      logout: '/auth/login'
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/cms/login',
            method: 'post',
            propertyName: ''
          },
          logout: { url: '/cms/logout', method: 'get' },
          user: { url: '/cms/user/auth', method: 'get', propertyName: 'data' }
        }
      }
    },
    plugins: ['~/plugins/checkPermission.js']
  },
  router: {
    middleware: ['auth', 'permission']
  }
}
