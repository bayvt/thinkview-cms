const { colors } = require('tailwindcss/defaultTheme')
const whitelister = require('purgecss-whitelister')

/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    container: {
      center: true,
      padding: {
        default: '1rem'
      }
    },
    extend: {
      colors: {
        primary: '#27CEBE',
        info: '#042261',
        dark: {
          default: '#011235',
          blue: '#000F2E'
        },
        pearl: '#03182d',
        gray: {
          ...colors.gray,
          dark: '#080D1B',
          default: '#F7FAFC',
          medium: '#878B96',
          blue: '#DEE1E9',
          '100': '#C3C5CB',
          '200': '#EFF0F4',
          '300': '#e2e8f0'
        }
      }
    }
  },
  variants: {},
  plugins: [],
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    // enabled: false,
    content: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'nuxt.config.js'
    ],
    // These options are passed through directly to PurgeCSS
    options: {
      whitelist: whitelister([
        './node_modules/element-ui/lib/theme-chalk/index.css'
      ])
    }
  }
}
