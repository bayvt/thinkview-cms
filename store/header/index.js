export const actions = {
  async searchHeader({}, body) {
    return await this.$axios.$post('/cms/folder/header/search', body)
  },
  async getTopHeader() {
    return await this.$axios.$get('/cms/folder/header/top_folders')
  }
}
