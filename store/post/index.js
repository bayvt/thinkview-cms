export const state = () => ({
  folders: [],
  setOnRecently: [],
  listOnRecently: [],
  keywordsRecently: []
})
export const getters = {
  folders: state => state.folders,
  setOnRecently: state => state.setOnRecently,
  listOnRecently: state => state.listOnRecently,
  keywordsRecently: state => state.keywordsRecently
}
export const mutations = {
  SET_FOLDERS(state, payload) {
    state.folders = payload
  }
}
export const actions = {
  async getFolders({ commit }) {
    const { data } = await this.$axios.$post('/cms/post/add/folder_search')
    commit('SET_FOLDERS', data)
  },
  async addPost({}, body) {
    return await this.$axios.$post('/cms/post/add', body, {
      headers: {}
    })
  }
}
