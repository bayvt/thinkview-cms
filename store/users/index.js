export const actions = {
  async getListUser({}, body) {
    return await this.$axios.$post('/cms/user/list', body)
  },
  async addUser({}, body) {
    return await this.$axios.$post('/cms/user/add', body)
  },
  async editUser({}, { id, ...body }) {
    return await this.$axios.$post('/cms/user/edit/' + id, {
      ...body
    })
  },
  async deleteUser({}, { id }) {
    return await this.$axios.$get('/cms/user/delete/' + id)
  }
}
