export const state = () => ({
  listRole: []
})
export const mutations = {
  SET_LIST_ROLE(state, payload) {
    state.listRole = payload
  }
}
export const actions = {
  async getListRole({ commit }) {
    const { data } = await this.$axios.$get('/cms/role/list')
    commit('SET_LIST_ROLE', data)
  },
  async addRole({}, body) {
    return await this.$axios.$post('/cms/role/add', body)
  },
  async editRole({}, { id, name, permissions }) {
    return await this.$axios.$post('/cms/role/edit/' + id, {
      name,
      permissions
    })
  },
  async deleteRole({}, { id }) {
    return await this.$axios.$get('/cms/role/delete/' + id)
  }
}
