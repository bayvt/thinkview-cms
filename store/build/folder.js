import cloneDeep from 'lodash/cloneDeep'

export const state = () => ({
  listFolder: []
})
export const getters = {
  listFolder: state =>
    cloneDeep(state.listFolder).filter(folder => folder.is_showed)
}
export const mutations = {
  SET_LIST_FOLDER(state, payload) {
    state.listFolder = payload
  }
}
export const actions = {
  async getListFolder({ commit }) {
    const { data } = await this.$axios.$get('/cms/folder/list')
    commit('SET_LIST_FOLDER', data)
  },
  async addFolder({}, body) {
    return await this.$axios.$post('/cms/folder/add', body)
  },
  async editFolder({}, { id, name, is_showed }) {
    return await this.$axios.$post('/cms/folder/edit/' + id, {
      name,
      is_showed
    })
  },
  async rearrangeFolder({ state }) {
    const body = {
      folders: state.listFolder
    }
    return await this.$axios.$post('/cms/folder/rearrange', body)
  }
}
