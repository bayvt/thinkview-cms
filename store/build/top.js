export const actions = {
  async getTopStory() {
    return await this.$axios.$get('/cms/build/top/get')
  }
}
