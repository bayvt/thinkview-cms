export const state = () => ({
  isCollapse: false
})
export const mutations = {
  SET_IS_COLLAPSE: (state, payload) => (state.isCollapse = payload)
}
export const getters = {
  isCollapse: state => state.isCollapse
}
