export default function({ $isPermission, route, error }) {
  const [meta] = route.meta
  const permission = meta?.permission
  if (!permission) return
  const isPermission = $isPermission(permission)
  if (!isPermission) {
    return error({
      statusCode: 503,
      message: ''
    })
  }
}
