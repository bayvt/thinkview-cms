import Vue from 'vue'
import SvPageHeader from '~/components/ui/SvPageHeader'
import SvButtonFixed from '~/components/ui/SvButtonFixed'

Vue.component(SvPageHeader.name, SvPageHeader)
Vue.component(SvButtonFixed.name, SvButtonFixed)
