import Vue from 'vue'
import Element, { Notification } from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'

Vue.use(Element, { locale })

Vue.prototype.$success = (message, title = 'Thành công') => {
  return Notification({
    type: 'success',
    message,
    title,
    duration: 2000
  })
}

Vue.prototype.$error = (message, title = 'Lỗi') => {
  return Notification({
    type: 'error',
    message,
    title,
    duration: 2000
  })
}

Vue.prototype.$info = (message, title = 'Thông báo') => {
  return Notification({
    type: 'info',
    message,
    title,
    duration: 2000
  })
}
