import createPersistedState from 'vuex-persistedstate'

export default async ({ store }) => {
  await window.onNuxtReady(() => {
    createPersistedState({
      key: 'vuex',
      paths: ['isCollapse', 'post']
    })(store)
  })
}
