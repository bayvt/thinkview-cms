export default function({ $axios }) {
  $axios.onError(error => {})
  $axios.onRequest(request => {})
  $axios.onResponseError(err => {
    const { status, data } = err.response
    let message = ''
    if (status === 422) {
      message = data[Object.keys(data)?.[0]]?.[0] || ''
    } else {
      message = data.message || ''
    }
    throw {
      statusCode: status,
      message
    }
  })
}
