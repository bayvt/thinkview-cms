import Vue from 'vue'

export default function(context) {
  const isPermission = permission => {
    const permissions = context.$auth.user?.permissions || []
    return permissions?.includes(permission)
  }
  Vue.prototype.$isPermission = isPermission
  context.$isPermission = isPermission
}
