export default {
  builds: {
    name: 'Build trang',
    permission: {
      add: 'Thêm mới khu vực',
      edit: 'Chỉnh sửa khu vực',
      list: 'Xem danh sách khu vực',
      delete: 'Xóa khu vực'
    }
  },
  folders: {
    name: 'Thư mục',
    permission: {
      add: 'Thêm mới thư mục',
      edit: 'Chỉnh sửa thư mục',
      list: 'Xem danh sách thư mục',
      delete: 'Xóa thư mục'
    }
  },
  posts: {
    name: 'Bài viết',
    permission: {
      add: 'Thêm mới bài viết',
      edit: 'Chỉnh sửa bài viết',
      list: 'Xem danh sách bài viết',
      delete: 'Xóa thành bài viết',
      publish: 'Duyệt bài viết'
    }
  },
  users: {
    name: 'Thành viên',
    permission: {
      add: 'Thêm mới thành viên',
      edit: 'Chỉnh sửa thành viên',
      list: 'Xem danh sách thành viên',
      delete: 'Xóa thành viên'
    }
  },
  roles: {
    name: 'Vai trò',
    permission: {
      add: 'Thêm mới nhóm quyền',
      edit: 'Chỉnh sửa nhóm phân quyền',
      list: 'Xem danh sách phân quyền',
      delete: 'Xóa nhóm phân quyền'
    }
  },
  tags: {
    name: 'Tag',
    permission: {
      add: 'Thêm mới tag',
      edit: 'Chỉnh sửa tag',
      list: 'Xem danh sách tag',
      delete: 'Xóa tag'
    }
  }
}
